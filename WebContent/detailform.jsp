<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"
    import="example.UserBean"
    import ="java.sql.*"
    import = "java.lang.Math"%>

<% UserBean currentUser = (UserBean) (session.getAttribute("currentSessionUser"));

String email=currentUser.getEmail();
String password=currentUser.getPassword();

%>

<!DOCTYPE html>
<html>
<head>
<title>My Profile</title>
<meta name="viewport" content="width = device-width, initial-scale = 1">
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="custom.css">
</head>
<body class="logins">
<div class="container" style="margin-top:100px;">
    <div class="login-page">
        <!-- multistep form -->
<form id="msform" action="LoginServlet" method="post">
	<!-- progressbar -->
	<ul id="progressbar">
		<li class="active">Create your account</li>
		<li>Conditions and Allergies</li>
		<li>Appointments</li>
	</ul>
	<!-- fieldsets -->
	<fieldset>
		<h2 class="fs-title">Create your account</h2>
		<h3 class="fs-subtitle">Add your account details</h3>
        
                <input type="file" id="pic" onchange="loadFile(event)"  ></input>
                <img id="output" class="disp" style="width: 150px; height:150px;margin:0 auto;position:center; border-radius:75px; margin-bottom:10px;">
    
                <input type="text" name="name" id="name" placeholder="Full Name"/>
                <span id="name_err"></span><br>
    
                <input type="date" name="dob" id="dob" placeholder="Choose your Birthdate" onClick="$(this).removeClass('placeholderclass')" class="dateclass placeholderclass"style="width:48%; float:left; margin-right:20px;" />
                
                <span id="age_err"></span>
    
                <select name="gender" id="gender" class="fields form-control" style="width:48%; float:left; ">
                    <option value="-" disabled  hidden="hidden" >Select Gender</option>
                    <option value="m">Male</option>
                    <option value="f">Female</option>
                    <option value="o">Other</option>
                </select>
                <span id="gender_err"></span><br>
             
                <textarea  name="address" id="address" class="form-control" rows="3" cols="40" placeholder="Address" style="background-color:#fff; border:1px solid gray; border-radius:4px; font-size:14px;"></textarea>
                <span id="address_err"></span><br>
                

		        <input type="button"  name="next" class="next action-button" value="Next" />

	</fieldset>
        
	<fieldset>
		<h2 class="fs-title">Conditions and Allergies</h2>
		<h3 class="fs-subtitle">Add your Chronic Conditions here</h3>
        
		<input class="con form-control" type="text" name="chronic" placeholder="Condition" style="width:80%; margin:20px;margin-top:10px; border:0px solid #ccc;border-radius: 3px;font-size: 13px; color: #2C3E50; float:left;"><br>
        <button type="button" class=" add1 btn btn-lg btn-danger" style=" " onclick="return add()" >+</button> <br>
        
        <hr style="border-color:#ccc; width:300px;align:center; margin-left:100px;margin-right:100px;">
        
        <h3 class="fs-subtitle">Add your Allergies here</h3>
        
        <input class="all form-control" type="text" name="allergy" placeholder="Allergy" style="width:80%; margin:20px;margin-top:10px; border:0px solid #ccc;border-radius: 3px;font-size: 13px; color: #2C3E50; float:left;"><br>
        <button type="button" class=" add1 btn btn-lg btn-danger" style=" " onclick="return adda()" >+</button> <br>
        
		
		<input type="button" name="previous" class="previous action-button" value="Previous" />
		<input type="button" name="next" class="next action-button" value="Next" />
	</fieldset>
	<fieldset>
		<h2 class="fs-title">Surgery Records</h2>
		<h3 class="fs-subtitle">Enter the records of your surgeries</h3>
        
		<input style="width:58%; float:left;" type="text" id="surgery_name" name="surgery_name" placeholder="Name of Surgery"/>
		<input style="width:38%; float:right;" type="date" name="surgery_date" placeholder="Date Performed" />
        <input style="width:58%; float:left;" type="text" name="facility_name" placeholder="Facility" />
        <input style="width:38%; float:right;" type="text" name="city" placeholder="City" />
            
		<textarea rows="2" cols="40" name="details" placeholder="Details"></textarea>
        <textarea rows="2" cols="40" name="side_effects" placeholder="Side Effects"></textarea>
        <textarea rows="2" cols="40" name="outcomes" placeholder="Outcomes"></textarea>   
         
        <!--  ***PARAMETER IDENTITFIES THAT USER IS NEWLY REGISTERED*** -->    
        <input type="hidden" id="page" name="page" value="detailform">
        <input type="hidden" id="email" name="email" value="<%=email%>">
		<input type="hidden" id="password" name="password" value="<%=password%>">
		  					
		<input type="button" name="previous" class="previous action-button" value="Previous" />
        <button type="submit" name="submit" class="btn btn-warning submit action-button" value="Submit">Submit</button>
            
       
	</fieldset>
</form>


</div>
    </div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<!-- jQuery -->
<script src="http://thecodeplayer.com/uploads/js/jquery-1.9.1.min.js" type="text/javascript"></script>
<!-- jQuery easing plugin -->
<script src="http://thecodeplayer.com/uploads/js/jquery.easing.min.js" type="text/javascript"></script>

<script>   
    function presence(p,message){
		var z=p.val();
		//alert(z);
		var mess;
		if(message==undefined)
			mess="Must be Filled Out";
		else
			mess=message;
		
		var e = "#"+$(p).attr('id')+"_err";
		//alert(e);
		
		if(z == "" || z == "null")
		{
			//alert("in");
			$(e).html(mess);
			return false;
		}
		else{
			$(e).html('');
			return true;
		}
	}                    // CHECK IF FILLED
    
    function is_num(p,message){                            // CHECK IF NUMBER 
		var mess;
		if(message==undefined)
			mess="Must be Numeric";
		else
			mess=message;
		var x=p.val();
		var e = "#"+$(p).attr('id')+"_err";
		var reg="^[0-9]*$";
		
		if(x.match(reg)){
			$(e).html('');
			return true;
		}
		else{
			$(e).html(mess);
			return false;
		}

	}
    
    function dropdown(p,message){                              // DROPDOWN GENDER CHECK

		var mess;
		if(message==undefined)
			mess="Please select one!";
		else
			mess=message;

		var z = p.val();
		var e = "#"+$(p).attr('id')+"_err";
		if(z == "-")
		{
			//alert("in");
			$(e).html(mess);
			return false;
		}
		else{
			$(e).html('');
			return true;
		}


	}
    
    function extension(fileext,allow,size,message,message2){     // UPLOAD FILE CHECK

		var ext="."+fileext.val().split('.').pop().toLowerCase();

		var e = "#"+$(fileext).parent().attr('id')+"_err";
		var mess;
		if(message==undefined)
			mess="Invalid extension";
		else
			mess=message;

		var mess2;
		if(message2==undefined)
			mess2="Invalid filesize! should be less than :";
		else
			mess2=message2;
		if(allow=="image")
			allowArray=['.jpg','.png','.gif'];
		else if(allow=="pdf")
			allowArray=['.pdf'];
		else if(allow=="document")
			allowArray=['.pdf','.doc','.docx','.txt'];
		
		if(allow==undefined)
			allowArray=['.pdf','.jpg','.png','.doc','.docx','.txt','.gif'];
		
		if($.inArray(ext, allowArray) == -1){
			$(e).html(mess);
			return false;
		}

		else{
			$(e).html('');
			//alert("valid extension");
			var iSize = ($(fileext)[0].files[0].size / 1024);
			iSize = (Math.round((iSize / 1024) * 100) / 100);
			//alert(iSize);
			if(iSize<size){
				
				return true;
			}
			else{
				$(e).html(mess2+size+" MB");
				return false;
			}
			
		}
		
	}

    var loadFile = function(event) {
  	$("#output").hide();
  	var fileext=$("#pic");
  	//flag=true;
  	var ext="."+$("#pic").val().split('.').pop().toLowerCase()
  	alert(ext);
	var ans2=extension(fileext,"image",2);
  	var e = "#"+$(fileext).parent().attr('id')+"_err";

  	if(ans2==true){
  		$(e).html("");
  		if(ext==".jpg" || ext==".png" || ext==".gif"){
  			var output = document.getElementById('output');

		    output.src = URL.createObjectURL(event.target.files[0]);
		      $("#output").show();
	        		$(e).html("");		    

		  /*  $("<img>").attr("src", $(output).attr("src")).load(function(){
             realWidth = this.width;
             realHeight = this.height;
	            if(realHeight<=1000 && realWidth<=2500)
	        	{
	        		$("#output").show();
	        		$(e).html("");
			   		
	        	}
	        	else{
	        		$(e).html("Incorrect dimensions");
	        		$("#output").hide();
	        	} 
			   
        	}); */

		}
		else
			$("#output").hide();
  	}
  	else
			$("#output").hide();
  };
    
    var count=1;
    var counta=1;
    $("#output").hide();
    $(document).ready(function(){
	$("#demo").hide();
    $("#output").hide();
	$(document).on("click",".rem", function () {
	
		//alert("in");
	   // var a=;
	    var K="#con"+$(this).attr('id');
	    alert(K);
	    $(K).remove();
	    $(this).remove();
	    return false;
	});

    
    $(document).on("click",".rema", function () {
	
		//alert("in");
	   // var a=;
	    var K="#all"+$(this).attr('id');
	    alert(K);
	    $(K).remove();
	    $(this).remove();
	    return false;
	});
        
    $(document).on("click","#1", function() {
      $("#demo").toggle();
        return false;
    });
});
    
    function add(){
	var temp=count+1;
    //alert("Add me hai");
        $('.con:last').after('<div class="con"><input type="text" name="chronic" id="con'+temp+'" class="col-xs-10 form-control" placeholder="Chronic'+temp+'" style="width:80%; margin:20px;margin-top:10px; border:0px; display:inline;"> <button type="button" class="rem col-xs-2 btn btn-md" id="'+temp+'" style=" height:30px; width:30px; display:inline; margin:15px;">-</button></div>');
	count++;
	//alert(count);
	
	return false;
}
 
function adda(){
	var tempa=counta+1;
    //alert("Add me hai");
        $('.all:last').after('<div class="all"><input type="text" name="allergy" id="all'+tempa+'" class="col-xs-10 form-control" placeholder="Allergy'+tempa+'" style="width:80%; margin:20px;margin-top:10px; border:0px; display:inline;"> <button type="button" class="rema col-xs-2 btn btn-md" id="'+tempa+'" style=" height:30px; width:30px; display:inline; margin:15px;">-</button></div>');
	counta++;
	//alert(count);
	
	return false;
}
    
    
//jQuery time
var current_fs, next_fs, previous_fs; //fieldsets
var left, opacity, scale; //fieldset properties which we will animate
var animating; //flag to prevent quick multi-click glitches

$(".next").click(function(){
    var ans=false;
    var a=$('#name');
    var b=$('#dob');
    var c=$('#gender');
    var d=$('#address')
    ans=presence(a) && dropdown(c) && presence(b) && presence(d) ;
        

    
    
    if(ans==true){
	if(animating) return false;
	animating = true;
	
	current_fs = $(this).parent();
	next_fs = $(this).parent().next();
	
	//activate next step on progressbar using the index of next_fs
	$("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");
	
	//show the next fieldset
	next_fs.show(); 
	//hide the current fieldset with style
	current_fs.animate({opacity: 0}, {
		step: function(now, mx) {
			//as the opacity of current_fs reduces to 0 - stored in "now"
			//1. scale current_fs down to 80%
			scale = 1 - (1 - now) * 0.2;
			//2. bring next_fs from the right(50%)
			left = (now * 50)+"%";
			//3. increase opacity of next_fs to 1 as it moves in
			opacity = 1 - now;
			current_fs.css({'transform': 'scale('+scale+')'});
			next_fs.css({'left': left, 'opacity': opacity});
		}, 
		duration: 800, 
		complete: function(){
			current_fs.hide();
			animating = false;
		}, 
		//this comes from the custom easing plugin
		easing: 'easeInOutBack'
	});
    }
});

$(".previous").click(function(){
	if(animating) return false;
	animating = true;
	
	current_fs = $(this).parent();
	previous_fs = $(this).parent().prev();
	
	//de-activate current step on progressbar
	$("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");
	
	//show the previous fieldset
	previous_fs.show(); 
	//hide the current fieldset with style
	current_fs.animate({opacity: 0}, {
		step: function(now, mx) {
			//as the opacity of current_fs reduces to 0 - stored in "now"
			//1. scale previous_fs from 80% to 100%
			scale = 0.8 + (1 - now) * 0.2;
			//2. take current_fs to the right(50%) - from 0%
			left = ((1-now) * 50)+"%";
			//3. increase opacity of previous_fs to 1 as it moves in
			opacity = 1 - now;
			current_fs.css({'left': left});
			previous_fs.css({'transform': 'scale('+scale+')', 'opacity': opacity});
		}, 
		duration: 800, 
		complete: function(){
			current_fs.hide();
			animating = false;
		}, 
		//this comes from the custom easing plugin
		easing: 'easeInOutBack'
	});
});
</script> 

    </body>
</html>
     
    