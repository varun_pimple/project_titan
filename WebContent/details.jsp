<%@ page language="java" 
	contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"
    import="example.UserBean"%>

<%
    if ((session.getAttribute("currentSessionUser") == null) || (session.getAttribute("currentSessionUser") == "")) {
%>
You are not logged in<br/>
<a href="index.jsp">Please Login</a>
<% } else { 
	//********************************************************************************
	//************ This file is the front end of the Profile Fill up Page ************
	//********************************************************************************
	
	UserBean currentUser = (UserBean) (session.getAttribute("currentSessionUser"));
	String num="";
	
%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
 
<!-- If IE use the latest rendering engine -->
<meta http-equiv="X-UA-Compatible" content="IE=edge">
 
<!-- Set the page to the width of the device and set the zoom level -->
<meta name="viewport" content="width = device-width, initial-scale = 1">
<title>My Profile</title>
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="custom.css">

</head>
<body>

 <div class="navbar navbar-default navbar-fixed-top"> 
	<div class="container">
		<div class="navbar-header">
		<button class="navbar-toggle " data-toggle="collapse" data-target="#navHeaderCollapse">

				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
      <a class="navbar-brand" href="#">Titan</a>
    </div>
        <div class="collapse navbar-collapse " id="navHeaderCollapse">
		<ul class="nav navbar-nav navbar-right">
			
			<li> <a href="#"> Login </a> </li>

			<li><a href="#">Register </a> </li>
      
			<li> <a href="contact.html">Contact us</a> </li>
			
            <li><img src="casey.jpg" id="casey" class="img-responsive img-thumbnail img-circle pull-left" alt="Responsive image" style="width:40px;height:40px;">
			</li>
		</ul>
		
		</div>

	</div>
  </div>
    <div class="container" style="margin-top:100px;">
       <form action="completeProfile.jsp">  
        <div class="row" style="margin-left:15px;margin-right:15px;">
        <div class="card"> 
            <div class="col-xs-4 "> <!-- Mobile first, can be used for Desktops -->
                
                <img src="casey.jpg" id="casey" class="img-responsive img-circle pull-left" style="width:260px;height:260px; padding:0px; margin:20px;">
                
                <input type="file" role="buttons" style="margin-left:60px;margin-bottom:30px; z-index: 1;" >
            </div>     
            
   <!--  <form action="completeProfile.jsp">   -->
            <div class="col-xs-4">
            <h3>Welcome </h3> <!-- <%=currentUser.getEmail() %></h3>   -->
            <input type="text" name="fullname" id="full" class="form-control" placeholder="Full Name" style="width:100%; border:0px;">
            <br>
            
                 <h3>Enter your address</h3>
                 <textarea  name="address" class="form-control" rows="5" cols="40" placeholder="Address" style="background-color:#f2f2f2; border:0px; border-radius:0px; font-size:16px;"></textarea>
             </div>
            
            <div class="col-xs-4 "> 
                <h3>Select your gender</h3>
                <select name="gender" id="bro" class="fields form-control" style="padding:10px;border-radius:0px;margin-bottom:0px;">
                    <option value="-" disabled selected hidden="hidden" >GENDER</option>
                    <option value="m">Male</option>
                    <option value="f">Female</option>
                    <option value="o">Other</option>
                </select>
                
                <br>
                <h3>Select your Birthdate</h3>
                <input name="dob" class="fields form-control" type="date" style="margin-top:0px;border-radius: 0px;">
            </div>
        </div> 
    </div>
    
    <div class="row" style="margin-left:0px; margin-right:0px;">
        <div class="col-xs-6 ">
            <div class="othercards">
                <h2 class="headings"> Chronic Conditions</h2> <br>
                 <input class="con form-control" type="text" name="chronic" placeholder="Condition 1" 
                 style="width:80%; margin:20px;margin-top:10px; border:0px;">
                 <button type="button" class="btn btn-lg btn-danger" onclick="return add()">+</button>
                  <% //System.out.println(count); %>
                 <input type="text" name="chronic2" class="form-control" placeholder="Condition 2" 
                 style="width:80%; margin:20px;margin-top:10px; border:0px;">
            </div>
        </div>

        <div class="col-xs-6 ">
            <div class="othercards">
                <h2 class="headings"> Allergies</h2> <br>
                <input type="text" name="allergy1" class="form-control" placeholder="Allergy 1" 
                 style="width:80%; margin:20px;margin-top:10px; border:0px;">
                 <button type="button" class="btn btn-lg btn-danger" onclick="return add2()">+</button>
                
                 <input type="text" name="allergy2" class="form-control" placeholder="Allergy 2" 
                 style="width:80%; margin:20px;margin-top:10px; border:0px;">
                <a href='logout.jsp'>Log out</a>
            </div>
        </div>
    </div>
     <button type="submit" class="btn btn-default" style="align:center; margin-left: 300px; margin-top: 20px; margin-bottom: 100px;">Submit</button>
   </form>
</div>
   
          
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>    
<script>
var count=1;
$(document).ready(function(){
	
	
	$(document).on("click",".rem", function () {
	
		alert("in");
	   // var a=;
	    var b="#con"+$(this).attr('id');
	    alert(b);
	    $(b).remove();
	    $(this).remove();
	    return false;
	});
});
function add(){
	var temp=count+1;
	$('.con:last').after('<input type="text" name="chronic" id="con'+temp+'" class="con col-xs-10 form-control" placeholder="Chronic'+temp+'" style="width:80%; margin:20px;margin-top:10px; border:0px;"><button type="button" class="rem col-xs-2 btn btn-md" id="'+temp+'" >-</button>  ');
	<% %>
	count++;
	alert(count);
	
	return false;
}
</script>
</body>
</html>

<% } %>