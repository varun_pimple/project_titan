<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Get Started</title>
<link href="samplelogin.css" rel="stylesheet">
<link rel="shortcut icon" href="">
<script>
function validateForm() {
    //for Username 
    var a = document.forms["myForm"]["username"].value;
    	if (a == null || a == "") {
        alert("Username should not be blank");
        return false;
   	 }
	 //for Password 
    var a = document.forms["myForm"]["password"].value;
    	if (a == null || a == "") {
        alert("Password should be filled out");
        return false;
   	 }
}
</script>

</head>
<body class="logins">
<div class="login-page">
	<p class="loginup">
		Get Started
	</p>
	Updated 03:01pm
  <div class="form">    
<%
  	String value=request.getParameter("newUser");
%>
      <form class="login-form" name="myForm" action="LoginServlet" onsubmit="return validateForm()" method="post">
          <input type="text" name="email" placeholder="Email"/> 
          <input type="password" name="password" placeholder="password"/>
          <button type="submit" value="Login">login</button>
 		<% if("yes".equals(value)){ %>
 			<input type="hidden" id="naya" name="naya" value="1">
 		<% } %>
          <p class="message">New User? <a href="register.jsp">Register Here</a>
          </p>
      </form>
  </div>
    
<!-- Made this go from (index.jsp)login.html to loginServlett.java to test.jsp -->    
    
    <div class="form1">
        <button class="Fbutton" type="submit">Sign In with Facebook</button>
        <button class="Gbutton" type="submit">Sign In with Google</button>
    </div>
</div>
 
</body>

</html>