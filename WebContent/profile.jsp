<%@ page language="java" 
contentType="text/html; charset=UTF-8"
pageEncoding="UTF-8"
import="example.UserBean"
import= "java.util.ArrayList"

%>

<%
if ((session.getAttribute("currentSessionUser") == null) || (session.getAttribute("currentSessionUser") == "")) {
%>
You are not logged in<br/>
<a href="index.jsp">Please Login</a>
<% } else { 
	//********************************************************************************
	//************ This file is the front end of the Profile Fill up Page ************
	//********************************************************************************
	
	UserBean currentUser = (UserBean) (session.getAttribute("currentSessionUser"));
	ArrayList<String> allergies =new ArrayList<String>();
	%>

	<!DOCTYPE html>
	<html lang="en">
	<head>

		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width = device-width, initial-scale = 1">
		<link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
		<link rel="stylesheet" type="text/css" href="css/bootstrap.css">    
		<!-- <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css"> -->
		<link type="text/css" rel="stylesheet" href="css/materialize.css"  media="screen,projection"/>
		<link rel="stylesheet" type="text/css" href="custom.css">
		<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400" rel="stylesheet">
	</head>
	<body id="body1">
		<div class="container">
			<div class="navbar navbar-default navbar-fixed-top" style="background-color:#303F9F;">
				<div class="navbar-header">
					<button class="navbar-toggle " data-toggle="collapse" data-target="#navHeaderCollapse">

						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="#" style="color:white;">Titan  </a>
				</div>
				<div class="collapse navbar-collapse " id="navHeaderCollapse">
					<ul class="nav navbar-nav navbar-right">

						<li> <a href="#"> Login </a> </li>

						<li><a href="dashboard.jsp">Dashboard </a> </li>

						<li> <a href="contact.html">Contact us</a> </li>

						<li><img src="varun.jpg" id="casey" class="" alt="Responsive image" style="width:40px;height:40px;">
						</li>
					</ul>
				</div>
			</div>
		</div>    

		<div class="container">
			<div class="row">
				<div class="col-xs-2"></div>
				<div class="col-xs-8" style="padding:0px;">
					<div id="cover">
						<div id="nameplate">  <!-- Actually the cover photo --></div>
						<div id="photo"> </div>
						<div id="name" align="center"> <b> <%=currentUser.getFullName()%> </b> </div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-2"></div> 
				<div class="col-xs-8 masonry" > 
					<div class="content">
						
						<span style="font-size: 20px;">About</span>
						<span id="about" class="glyphicon glyphicon-pencil" style="float: right;margin-top: 5px;"></span> 
						<hr style="margin-top: 7px;">
						<label>Age</label>
						<input type="number" value="<%=currentUser.getDOB()%> " readonly="readonly" class="dispab">
						<label>Gender</label>
						<% String g = currentUser.getGender();
						String gender="";
						if("m".equals(g)){
						gender="Male";
					}
					if("f".equals(g)){
					gender="Female";
				}
				%>
				<input type="text" value="<%=gender %> " readonly="readonly" class="dispab">

			</div>

			<div class="content">
				<span style="font-size: 20px;">Allergies</span>
				<span id="allergies" class="glyphicon glyphicon-pencil" style="float: right;margin-top: 5px;"></span> 
				<hr style="margin-top: 7px;">
				<form name="yourForm2" action="LoginServlet" method="post">
					<input type="hidden" name="email" value="<%=currentUser.getEmail()%>">
					<input type="hidden" name="password" value="<%=currentUser.getPassword()%>">
					<input type="hidden" name="page" value="AllergiesCardProfile">
					<%
					
					
					allergies= currentUser.getAllergies();
					int len=allergies.size();
					System.out.println("Allergy size"+len);
					for(int i=0;i<len;i++){
					%>
					<input type="text" name="allergies" value="<%=allergies.get(i)%>" readonly="readonly" class="dispall">
					<% 	
				} 
				allergies.clear();%>  
			</form>

		</div>
		<div class="content">
			<span style="font-size: 20px;">Records Of Surgery</span>
			<!-- <span id="about" href="#" class="glyphicon glyphicon-pencil" style="float: right;margin-top: 5px;"></span>  -->
			<hr style="margin-top: 7px;">
			<p style="display: inline;font-size: 15px;">Endometriosis</p>
			<a style="float: right;" href="#updatesurgery" class="modal-trigger waves-effect waves-light btn btn-danger">View Details
			</a>
		</div>
		<div class="content">
			<form name="yourForm" action="LoginServlet" method="post">
				<span style="font-size: 20px;">Details</span>
				<span id="details" class="glyphicon glyphicon-pencil" style="float: right;margin-top: 5px;"></span> 
				<hr style="margin-top: 7px;">
				<input type="hidden" name="page" value="DetailsCardProfile">
				<input type="hidden" name="password" value="<%=currentUser.getPassword()%>">
				
				<label>Email-Address</label>
				<input type="text" value="<%=currentUser.getEmail()%>" name="email" class="dispdetails" readonly="readonly">
				<label>Address</label>
				<input type="text" value="<%=currentUser.getAddress()%>" name="address"  class="dispdetails" readonly="readonly">
				<label>Phone-number</label>
				<input type="text" value="<%=currentUser.getPhoneNumber()%>"  name="mobno" class="dispdetails" readonly="readonly">
				<!-- <button id="submitdetails" type="submit">SAVE</button> -->
			</form>
		</div>

		<div class="content">
			<span style="font-size: 20px;">Conditions</span>
			<a id="Conditions" class="glyphicon glyphicon-pencil" style="float: right;margin-top: 5px;"></a> 
			<hr style="margin-top: 7px;">
			<form name="yourForm1" action="LoginServlet" method="post">
				<input type="hidden" name="email" value="<%=currentUser.getEmail()%>">
				<input type="hidden" name="password" value="<%=currentUser.getPassword()%>">
				<input type="hidden" name="page" value="ConditionsCardProfile">
				<%
				ArrayList<String> conditions = currentUser.getConditions();
				int len1 = conditions.size();
				for(int i=0;i<len1;i++){
				%>
				<input type="text" name="conditions" value="<%=conditions.get(i)%>" readonly="readonly" class="dispcon">
				<%
			}
			conditions.clear();%>   
		</form>
	</div>


</div>
</div>

<div class="fixed-action-btn vertical click-to-toggle">
	<a class="btn-floating btn-large red">
		<i class="material-icons">add</i>
	</a>
	<ul>
		<li><a data-position="left" data-delay="50" data-tooltip="Surgery Record" class=" tooltipped modal-trigger waves-effect waves-light btn-floating red" href="#modal1"><i class="material-icons">insert_chart</i></a></li>
	</ul>
</div>

<div id="modal1" class="modal" style="overflow-y: auto;">
	<div class="modal-content">
		<h4>Record of Surgery</h4>
		<form action="LoginServlet" method="post">
			<div class="input-field">
				<label for="Surgery">Name of Surgery</label>
				<input type="text" name="surgery_name" id="Surgery">
			</div>
			<div class="row s12">
				<div class="input-field col s6">
					<label for="date" class="active">Date Performed</label>
					<input type="date" name="surgery_date" id="#date" placeholder="">
				</div>
				<div class="input-field col s6">
					<label for="facility">Facility</label>
					<input type="text" name="facility_name" id="facility">
				</div>
			</div>
			<div class="row">
				<div class="input-field col s6">
					<label for="#city">City</label>
					<input type="text" name="city" id="city">
				</div>
				<div class="input-field col s6">
					<label for="#details">Details</label>
					<input type="text" name="details" id="details">
				</div>
			</div>
			<div class="row">
				<div class="input-field col s6">
					<label for="#side">SideEffects</label>
					<input type="text" name="side_effects" id="side">
				</div>
				<div class="input-field col s6">
					<label for="#Outcome">Outcome</label>
					<input type="text" name="outcomes" id="Outcome">
				</div>
			</div>
			<input type="hidden" name="email" value="<%=currentUser.getEmail()%>">
			<input type="hidden" name="password" value="<%=currentUser.getPassword()%>">
			<input type="hidden" name="patient_id" value="<%=currentUser.getPatientID()%>">
			<input type="hidden" name="page" value="AddRecordModal">
   <!--  </div>
   <div class="modal-footer"> -->
   	<button class=" modal-action modal-close waves-effect waves-green btn-flat" type="submit">Save</button>
   	<span class=" modal-action modal-close waves-effect waves-green btn-flat">Cancel</span>
   </form>
</div>
</div>

<div id="updatesurgery" class="modal" style="overflow-y: auto;">
	<div class="modal-content">
		<h4>Record of Surgery</h4>
		<form>
			<div class="input-field">
				<label for="Surgery">Name of Surgery</label>
				<input type="text" id="Surgery">
			</div>
			<div class="row s12">
				<div class="input-field col s6">
					<label for="date" class="active">Date Performed</label>
					<input type="date" id="#date" placeholder="">
				</div>
				<div class="input-field col s6">
					<label for="facility">Facility</label>
					<input type="text" id="facility">
				</div>
			</div>
			<div class="row">
				<div class="input-field col s6">
					<label for="#city">City</label>
					<input type="text" id="city">
				</div>
				<div class="input-field col s6">
					<label for="#details">Details</label>
					<input type="text" id="details">
				</div>
			</div>
			<div class="row">
				<div class="input-field col s6">
					<label for="#side">SideEffects</label>
					<input type="text" id="side">
				</div>
				<div class="input-field col s6">
					<label for="#Outcome">Outcome</label>
					<input type="text" id="Outcome">
				</div>
			</div>
		</form>
   <!--  </div>
   <div class="modal-footer"> -->
   	<span class=" modal-action modal-close waves-effect waves-green btn-flat">Edit</span>
   	<span class=" modal-action modal-close waves-effect waves-green btn-flat">Save</span>
   </div>
</div>

</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<!-- <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>    -->
<script type="text/javascript" src="js/bootstrap.js"></script>
<script type="text/javascript" src="js/materialize.min.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
	// the "href" attribute of .modal-trigger must specify the modal ID that wants to be triggered
	$('.modal').modal();
});
</script>
<script type="text/javascript">
	$(document).ready(function() {


		$('#about').on('click',function() {

			$('.dispab').attr('readonly' , false);
			$('.dispab:last').after( '<button id="submitab" type="submit" class="btn btn-danger">Save Changes</button>');
		 // $("#about").off("click");
		});
		$('#details').on('click',function() {

			$('.dispdetails').attr('readonly' , false);
			$('.dispdetails:last').after( '<button id="submitdetails" type="submit" class="btn btn-danger">Save Changes</button>');
		 // $("#about").off("click");
		});
		$('#allergies').on('click',function() {

			$('.dispall').attr('readonly' , false);
			$('.dispall:last').after( '<button id="addallergy" type="button" class="btn btn-danger">Add</button><button style="margin-left:5px;" type="submit" id="submitallergy" class="btn btn-danger">Save Changes</button>');
		 // $("#about").off("click");
		});
		$('#Conditions').on('click',function() {

			$('.dispcon').attr('readonly' , false);
			$('.dispcon:last').after( '<button id="addcondition" type="button" class="btn btn-danger">Add</button><button style="margin-left:5px;" type="submit" id="submitcondition" class="btn btn-danger">Save Changes</button>');
		 // $("#about").off("click");
		});

	});
//hi one second i need to make a achnage
$(document).on('click', '#submitab', function()
{

	$('.dispab').attr('readonly' , true);
	$(this).remove();

		//$("#about").bind("click",eventhandler);
	});
 // $(document).on('click', '#submitdetails', function()
	//{

	//$('.dispdetails').attr('readonly' , true);
	//$(this).remove();

		//$("#about").bind("click",eventhandler);
	  //});
	  $(document).on('click', '#addallergy', function()
	  {
	  	$('.dispall:last').after( '<input type="text" name="allergies" class="dispall">');
	  });

	  $(document).on('click', '#addcondition', function()
	  {
	  	$('.dispcon:last').after( '<input type="text" name="conditions" class="dispcon">');
	  });
	  
	</script>
</body>
</html>


<% }
%>