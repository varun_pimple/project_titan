<%@ page language="java" 
	contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"
    import="example.UserBean"%>

<%
    if ((session.getAttribute("currentSessionUser") == null) || (session.getAttribute("currentSessionUser") == "")) {
%>
You are not logged in<br/>
<a href="index.jsp">Please Login</a>
<% } else { 
	//********************************************************************************
	//************ This file is the front end of the Profile Fill up Page ************
	//********************************************************************************
	
	UserBean currentUser = (UserBean) (session.getAttribute("currentSessionUser"));
	
%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
 
<!-- If IE use the latest rendering engine -->
<meta http-equiv="X-UA-Compatible" content="IE=edge">
 
<!-- Set the page to the width of the device and set the zoom level -->
<meta name="viewport" content="width = device-width, initial-scale = 1">
<title>My Dashboard</title>
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="custom.css">

</head>
<body id="body1">

<div class="container">
        <div class="navbar navbar-default navbar-fixed-top">
		<div class="navbar-header">
		<button class="navbar-toggle " data-toggle="collapse" data-target="#navHeaderCollapse">

				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
      <a class="navbar-brand" href="#" style="color:white;">Titan</a>
    </div>
        <div class="collapse navbar-collapse " id="navHeaderCollapse">
		<ul class="nav navbar-nav navbar-right">
			
			<li> <a href="#"> Login </a> </li>

			<li><a href="#">Register </a> </li>
      
			<li> <a href="contact.html">Contact us</a> </li>
			
            <li><img src="casey.jpg" id="casey" class="img-responsive img-thumbnail img-circle pull-left" alt="Responsive image" style="width:40px;height:40px;">
			</li>
		</ul>
		
		</div>
  </div>
  </div>
    <br><br><br>
    
    <div class="container">
        <div class="row">
            <div class="col-xs-1 "></div>
            <div class="col-xs-10 "> <!-- Mobile first, can be used for Desktops -->
    
        <ul class="nav nav-pills">
            <li class="active"><a data-toggle="pill" href="#home">UPCOMING</a></li>
            <li><a data-toggle="pill" href="#menu1">VACCINATIONS</a></li>
            <li><a data-toggle="pill" href="#menu2">OTHER</a></li>
        </ul>
            </div>
        </div>
  <div class="row">
        <div class="col-xs-1 "></div>
        <div class="col-xs-10 ">
  <div class="tab-content" style="border:0px; ">
      
    <div id="home" class="tab-pane fade in active" style="2">
        <div class="upcomingcards">
             <h3>Welcome <%=currentUser.getFullName() %>  </h3>
             <h4>You stay at <%=currentUser.getAddress() %> </h4>
        </div>
        <div class="upcomingcards">         
            <a href="logout.jsp"> Logout here</a>
    </div>
    </div>
            
    <div id="menu1" class="tab-pane fade">
      <h3>User Details</h3>
    <div class="upcomingcards">
        <h4>Welcome <%=currentUser.getFullName() %>  </h4>
        <h4>You stay at <%=currentUser.getAddress() %> </h4>
    </div>
            
    <div id="menu2" class="tab-pane fade">
      <h3>Menu 2</h3>
      <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam.</p>
    </div>
 
     
     
       </div>
    </div>
</div>
</div>
</div>
<!-- 
<ul class="nav nav-pills nav-justified">
  <li class="active"><a href="#">UPCOMING</a></li>
  <li><a href="#">VACCINATIONS</a></li>
  <li><a href="#">PRESCRIPTION</a></li>
</ul>
Centered Pills -->
   
          
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>    
</body>
</html>

<% }
%>