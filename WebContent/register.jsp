<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
   <!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Register</title>
<link href="samplelogin.css" rel="stylesheet">

 



</head>
<body class="logins">
    <div class="login-page">
        <p class="loginup">
            Sign Up
        </p>


  <div class="form formSign">
    <form  name="myForm" action="LoginServlet" method="post" onsubmit="return validate()" >     
        
        
        <input type="text" id="email" name="email" placeholder="E-mail"> 
        <span id="email_err"></span><br>
        <input type="text" id="mobno" name="mobno" placeholder="Mobile Number"> 
        <span id="mobno_err"></span><br>
        <input type="password" id="password" name="password" placeholder="Enter new password">
        <input type="password" id="password1" name="password1" placeholder="Re-enter password">
        <span id="password1_err"></span><br>
        

		<input type="hidden" id="page" name="page" value="register">
        <button type="submit" name="submit" >Next <!-- <a href="otp.html"> </a> --> </button>
        <p class="message">Already registered? <a href="index.jsp">Sign In</a> </p>
    </form>
    
  </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script>
              
    function validate(){
    alert("Hellu");
    var a=$('#email');
    var b=$('#mobno');
    var c=$('#password');
    var d=$('#password1');
   // var ans=presence(a) && email_check(a) && presence(b) && is_num(b,"Must be a 10 digit phone number") && equality(c,d,"Passwords don't match!");
    var ans=false;
    if(presence(a)){
    // alert("Email present");
        if(email_check(a)){
        //alert("Email checked");
            if(presence(b)){
            //alert("Number present");
                if(is_num(b)){
                    if(length(b,10,"equal")){
                    //alert("Number checked");
                        if(equality(c,d)){
                        //alert("Password match");
                            ans=true;
                    }}}}}}
        
    //alert(ans);
    if(ans==true){      
    
 	alert("Success :)");
    return true;
    }
    else{
        alert("Account creation failed. Please try again.");
    return false;
    
    }
}
                                   
function equality(source,dest,message){                // MATCH PASSWORDS
		var m=source.val();
		var n=dest.val();
		var e = "#"+$(dest).attr('id')+"_err";
        //alert(e);
		var mess;
		if(message==undefined)
			mess="Not a match";
		else
			mess=message;
		if(m==n){
			$(e).html('');
			return true;
		}
		else {
			$(e).html(mess);
            alert("Equality is false");
			return false;
		}
	}	
    
function presence(p,message){                      //IF IT IS FILLED OUT
		var z=p.val();
		//alert(z);
		var mess;
		if(message==undefined)
			mess="Must be Filled Out";
		else
			mess=message;
		
		var e = "#"+$(p).attr('id')+"_err";
		//alert(e);
		
		if(z == "" || z == "null")
		{
			//alert("in");
			$(e).html(mess);
            alert("Presence is false");
			return false;
		}
		else{
			$(e).html('');
			return true;
		}
	}
    
function is_num(p,message){                            // CHECK IF NUMBER 
		var mess;
		if(message==undefined)
			mess="Must be Numeric";
		else
			mess=message;
		var x=p.val();
		var e = "#"+$(p).attr('id')+"_err";
		var reg="^[0-9]*$";
		
		if(x.match(reg)){
			$(e).html('');
			return true;
		}
		else{
			$(e).html(mess);
            alert("is_num is false");
			return false;
		}

	}
    
function email_check(em,seperator,message){            // EMAIL CHECK
		var mess;
		if(message==undefined)
			mess="Invalid email:";
		else
			mess=message;
		if(seperator==undefined)
			seperator=",";
		var x = em.val();
		var emailArray = x.split(seperator);
		var erremails="";
		var e = "#"+$(em).attr('id')+"_err";
		var reg=/^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
		var flag=true;
				//alert(emails);
		var i;
				
				for(i=0;i<(emailArray.length);i++)
				{
					if(emailArray[i].match(reg))
						continue;
					else
					{
						 erremails+=emailArray[i]+' ,';
						flag=false;
						continue;
						
						
					}
				}
       
         if(flag==true){
         	$(e).html('');
         	return true;
         }
         	
         else{
         	$(e).html(mess+erremails);
         	return false;
         } 
         	
	}
    
function length(p,len,type,message){               // FOR LENGTH OF PASSWORD ATLEAST, ATMOST
		var mess;
		if(message==undefined)
			mess="Must be of ";
		else
			mess=message;
		var z=p.val();
		var e = "#"+$(p).attr('id')+"_err";
		if(type=="equal")
		{
			if(z.length == len){
				$(e).html('');
				return true;
			}
			else
			{
				$(e).html(mess+len+' characters');
				return false	
			}
		}
		else if(type=="atleast")
		{
			if(z.length>=len){
				$(e).html('');
				return true;
			}
			else{
				$(e).html(mess+' atleast '+len+' characters');
				return false;
			}
		}
		else if(type=="atmost")
		{
			if(z.length<=len){
				$(e).html('');
				return true;
			}
			else{
				$(e).html(mess+' atmost '+len+' characters');
				return false;
			}
		}
		
	}
    


</script>      
    
</body>
</html>