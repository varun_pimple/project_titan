package example;
import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import example.UserBean;
import java.sql.*;
import java.util.ArrayList;

@WebServlet("/LoginServlet")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	UserBean user = new UserBean();
	static Connection currentCon = null;

	public LoginServlet() {
		super();
		// TODO Auto-generated constructor stub
	}
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());	
	}
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, NullPointerException {
		//doGet(request, response);
		try
		{		
			System.out.println("Back in Login Servlet");
			
			HttpSession session = request.getSession(false);
			if (session == null) {
				// Not created yet. Now do so yourself.
				session = request.getSession();
				System.out.println("Session got created");
			} else {
				// Already created.
				System.out.println("Session already exists");
			}

			String page = request.getParameter("page");
			
			/**
			This part checks which page the control has come from
			*/
			if("detailform".equals(page)){
				System.out.println("calling completeprofile() function");
				completeprofile(request,response);
				System.out.println("done with completeprofile() function");
			}
			else if("AddRecordModal".equals(page)){
				System.out.println("calling add surgery record function");				 
				addsurgery(request,response);
			}
			else if("DashboardAddAppointment".equals(page)){
				System.out.println("calling add appointment function");				 
				addappointment(request,response);
			}
			else if("DetailsCardProfile".equals(page)){
				System.out.println("calling update details function");
				String email = request.getParameter("email"); 
				String address =  request.getParameter("address");
				String phone = request.getParameter("mobno");
				//String password = request.getParameter("password");
				System.out.println(address);
				updateDetails(request,response,email,address,phone);	
			}
			else if("ConditionsCardProfile".equals(page)){
				System.out.println("calling update condition function");				 
				String[] conditions = request.getParameterValues("conditions");
				updateconditions(request,response,conditions);
			}
			else if("AllergiesCardProfile".equals(page)){
				System.out.println("calling update allergies function");				 
				String[] allergies = request.getParameterValues("allergies");
				updateallergies(request,response,allergies);
			}
			else if("register".equals(page)){
				String email = request.getParameter("email");    
				String pwd = request.getParameter("password");
				String mobno = request.getParameter("mobno");
				register(request,response,email,pwd,mobno);
				return;
			}

			String u="";
			u = request.getParameter("naya");
			System.out.println("Naya ka value is"+u);

			user.setEmail(request.getParameter("email"));
			user.setPassword(request.getParameter("password"));
			user = UserDAO.login(user);	 					// FOR LOGIN CREDENTIAL VALIDATION 

			if (user.isValid())								
			{
				session.setAttribute("currentSessionUser",user);	
				System.out.println("You are in isVALID");
				 if("1".equals(u)){    							// CHECK IF NEWLY REGISTERED USER
				 	u="";
				 	System.out.println("Forward to Multistep form...");
					 //String nextPage = "/detailform.jsp";
					 //RequestDispatcher rd1 = getServletContext().getRequestDispatcher(nextPage);
					 //rd1.forward(request,response);
				 	response.sendRedirect("detailform.jsp");
				 }
				 else{
				 	System.out.println("Still In LoginServlet, existing user, now fill up");
					 user = UserDAO.fillUp(user);				// FILL UP BEAN WITH ******ALL******* INFORMATION
					 session.setAttribute("currentSessionUser",user);

					 if("DashboardAddAppointment".equals(page))
					 	response.sendRedirect("dashboard.jsp");
					 else
						 response.sendRedirect("profile.jsp");  //profile    
					}
				}   
				else
				{ 
					System.out.println("You are in ELSE");
					response.sendRedirect("invalidLogin.jsp"); // Make this page 
			}
		} 


		catch (Throwable theException) 	    
		{
			System.out.println(theException); 
		}	
	}

	public void completeprofile(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, SQLException, ClassNotFoundException, NullPointerException{

		HttpSession session = request.getSession(false);
		if (session == null) {
			// Not created yet. Now do so yourself.
		   // session = request.getSession();
			System.out.println("Session is null in completeprofile function");
			System.out.println("creating a session inside function");
			session = request.getSession();
		}


		//UserBean currentUser = (UserBean) (session.getAttribute("currentSessionUser")); //VERY IMPORTANT
		System.out.println("passed currentSessionUser(session) into currentUser(userbean)");
		System.out.println("LoginServlet -> completeprofile");
//    	Getting everything from the Form and putting it into variables
		String address = request.getParameter("address");    
		String fullname = request.getParameter("name");
		String gender = request.getParameter("gender");
		String dob = request.getParameter("dob");					
		String[] conditions =request.getParameterValues("chronic");
		String[] allergies = request.getParameterValues("allergy");
		System.out.println("***No of Conditions are:");
		int conditionlength=conditions.length;
		int allergylength=allergies.length;
		System.out.print(conditions.length);  
		System.out.println();
// 	Need to add everthing into the bean before we pass bean into a DAO Function	
		System.out.print(address+"-- "+fullname+" --"+gender+"-- "+dob);		

		user.setFullName(fullname);
		System.out.println("full name set");
		user.setAddress(address);
		System.out.println("address set");
		user.setGender(gender);
		System.out.println("gender set");
		user.setDOB(dob);
		System.out.println("dob set");

		//ArrayList<String> conditionslist = (ArrayList<String>) Arrays.asList(conditions);
		ArrayList<String> conditionslist = new ArrayList<String>();
		for(int q=0;q<conditionlength;q++)
			conditionslist.add(conditions[q]);
		user.setConditions(conditionslist);
		System.out.println("condiiton set");

		//ArrayList<String> allergylist = (ArrayList<String>) Arrays.asList(allergies);			gave ClassCastException
		ArrayList<String> allergylist = new ArrayList<String>();
		for(int q=0;q<allergylength;q++)
			allergylist.add(allergies[q]);
		user.setAllergies(allergylist);

		System.out.println("allergy set");
		System.out.println("LoginServlet -> completeprofile -> values set into userbean and calling DAO");

		user=UserDAO.completeProfile(user);
	}

	public void register(HttpServletRequest request, HttpServletResponse response, String email,String pwd,String mobno) throws ServletException, IOException, SQLException, ClassNotFoundException, NullPointerException{

		HttpSession session = request.getSession(false);
		if (session == null) {
			// Not created yet. Now do so yourself.
		   // session = request.getSession();
			System.out.println("Session is null in completeprofile function");
			System.out.println("creating a session inside function");
			session = request.getSession();
		}
		user.setEmail(email);
		user.setPassword(pwd);
		user.setPhoneNumber(mobno);
		System.out.println("LoginServlet -> register");
		user=UserDAO.register(user);
		boolean flag = user.Registered();
		if (flag){
			//response.sendRedirect("welcome.jsp");
			String nextPage = "/welcome.jsp";
			RequestDispatcher rd = getServletContext().getRequestDispatcher(nextPage);
			rd.forward(request,response); 
			return; 
		}
		else {
			//response.sendRedirect("index.jsp");
			String nextPage = "/index.jsp";
			RequestDispatcher rd = getServletContext().getRequestDispatcher(nextPage);
			rd.forward(request,response);
			return; 

		}
	}
	public void updateDetails(HttpServletRequest request, HttpServletResponse response, String email, String address, String phone) throws ServletException, IOException, SQLException, ClassNotFoundException, NullPointerException{


		System.out.println("In the Servlet function to update");
		HttpSession session = request.getSession(false);
		if (session == null) {
			// Not created yet. Now do so yourself.
			// session = request.getSession();
			System.out.println("Session is null in completeprofile function");
			System.out.println("creating a session inside function");
			session = request.getSession();
		}
		user.setAddress(address);
		user.setEmail(email);
		user.setPhoneNumber(phone);
		System.out.println(address);
		System.out.println("After being set into bean object");

		user=UserDAO.updateDetails(user);

	}

	public void updateconditions(HttpServletRequest request, HttpServletResponse response, String[] conditions) throws ServletException, IOException, SQLException, ClassNotFoundException, NullPointerException{


		System.out.println("In the Servlet function to update");
		HttpSession session = request.getSession(false);
		if (session == null) {
			// Not created yet. Now do so yourself.
			// session = request.getSession();
			System.out.println("Session is null in completeprofile function");
			System.out.println("creating a session inside function");
			session = request.getSession();
		}
		int conditionlength=conditions.length;
		System.out.println("COndition Length in Servlet is"+conditionlength);
		ArrayList<String> conditionslist = new ArrayList<String>();
		for(int q=0;q<conditionlength;q++)
			conditionslist.add(conditions[q]);
		user.setConditions(conditionslist);
		System.out.println("condiiton set");

		//user.setEmail(email);

		System.out.println("After being set into bean object");

		user=UserDAO.updateConditions(user);

	}
	public void updateallergies(HttpServletRequest request, HttpServletResponse response, String[] allergies) throws ServletException, IOException, SQLException, ClassNotFoundException, NullPointerException{


		System.out.println("In the Servlet function to update");
		HttpSession session = request.getSession(false);
		if (session == null) {
		// Not created yet. Now do so yourself.
		// session = request.getSession();
			System.out.println("Session is null in completeprofile function");
			System.out.println("creating a session inside function");
			session = request.getSession();
		}
		int allergieslength=allergies.length;
		System.out.println("COndition Length in Servlet is"+allergieslength);
		ArrayList<String> allergieslist = new ArrayList<String>();
		for(int q=0;q<allergieslength;q++)
			allergieslist.add(allergies[q]);
		user.setAllergies(allergieslist);
		System.out.println("condiiton set");
		System.out.println("After being set into bean object");

		user=UserDAO.updateAllergies(user);

	}

	public void addsurgery(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, SQLException, ClassNotFoundException, NullPointerException{

		System.out.println("In the Servlet function to add surgery record");
		HttpSession session = request.getSession(false);
		if (session == null) {
		// Not created yet. Now do so yourself.
		// session = request.getSession();
			System.out.println("Session is null in completeprofile function");
			System.out.println("creating a session inside function");
			session = request.getSession();
		}


		String surgery_name = request.getParameter("surgery_name");
		String surgery_date = request.getParameter("surgery_date");
		String facility_name = request.getParameter("facility_name");
		String city = request.getParameter("city");
		String details = request.getParameter("details");
		String side_effects = request.getParameter("side_effects");
		String outcomes = request.getParameter("outcomes");
		user.setSurgeryName(surgery_name);
		user.setSurgeryDate(surgery_date);
		user.setSurgeryFacility(facility_name);
		user.setSurgeryCity(city);
		user.setSurgeryDetails(details);
		user.setSurgeryEffects(side_effects);
		user.setSurgeryOutcomes(outcomes);

		user=UserDAO.addsurgery(user);

	}

	public void addappointment(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, SQLException, ClassNotFoundException, NullPointerException{


		System.out.println("In the Servlet function to add appointment");
		HttpSession session = request.getSession(false);
		if (session == null) {
		// Not created yet. Now do so yourself.
		// session = request.getSession();
			System.out.println("Session is null in completeprofile function");
			System.out.println("creating a session inside function");
			session = request.getSession();
		}
		ArrayList<String> doctor_name = new ArrayList<String>();
		ArrayList<String> app_date = new ArrayList<String>();
		ArrayList<String> medicines = new ArrayList<String>();
		ArrayList<String> precautions = new ArrayList<String>();


		doctor_name.add(request.getParameter("doctor_name"));
		app_date.add(request.getParameter("app_date"));
		medicines.add(request.getParameter("medicines"));
		precautions.add(request.getParameter("precautions"));

		user.setDoctorName(doctor_name);
		user.setAppointment(app_date);
		user.setMedicines(medicines);
		user.setPrecautions(precautions);


		user=UserDAO.addappointment(user);

	}

}

