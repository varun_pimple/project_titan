package example;

import java.util.ArrayList;
import java.io.Serializable;

public class UserBean implements Serializable{
	

	private static final long serialVersionUID = -7686369854265971794L;
	private int patient_id;
	private String email;
	private String password;
	private String phone_number;
	private String full_name;
	private String address;
	private String gender;
	private String dob;
	private String naya;
	public boolean valid;
	public boolean register;
	
	private ArrayList<String> conditions = new ArrayList<>();
	private ArrayList<String> allergies = new ArrayList<>();
	private String surgery_name; 
	private String surgery_date; 
	private String facility_name; 
	private String city;
	private String details;
	private String side_effects;
	private String outcomes;
	private ArrayList<String> doctor_name = new ArrayList<>();
	private ArrayList<String> app_date = new ArrayList<>();
	private ArrayList<String> medicines = new ArrayList<>();
	private ArrayList<String> precautions = new ArrayList<>();

	public UserBean(){
	}
	
	public int n1=0,n2=0,n3=0,n4=0,n5=0,n6=0,n7=0,n8=0,n9=0;

	
	//**************************************************************************
	// ************ This file defines ALL the variables and values.*************  
	//**************************************************************************
	
	public ArrayList<String> getConditions() {  // Conditions
		return conditions;
	}
	public void setConditions(ArrayList<String> conditionList) {
		conditions=conditionList;
	}
	
	public void setAllergies(ArrayList<String> allergyList) {
		allergies=allergyList;
	}
	public ArrayList<String> getAllergies() {  // Allergies
		return allergies;
	}

	public void setDoctorName(ArrayList<String> doctorname) {
		doctor_name=doctorname;
	}
	public ArrayList<String> getDoctorName() {  // Doctor Name
		return doctor_name;
	}
	
	public void setAppointment(ArrayList<String> appdate) {
		app_date=appdate;
	}
	public ArrayList<String> getAppointment() {  // Appointment Date
		return app_date;
	}
	
	public void setMedicines(ArrayList<String> meds) {
		medicines=meds;
	}
	public ArrayList<String> getMedicines() {  // medicines
		return medicines;
	}
	
	public void setPrecautions(ArrayList<String> caution) {
		precautions=caution;
	}
	public ArrayList<String> getPrecautions() {  // precautions
		return precautions;
	}
	
	//----------------------------
	public String getSurgeryName() {  // Surgery Names
		return surgery_name;
	}
	public void setSurgeryName(String newSurgery) {
		surgery_name=newSurgery; //to be changed like upar waala
	}
	
	public String getSurgeryDate() {  // Surgery Date
		return surgery_date;
	}
	public void setSurgeryDate(String newSurgeryDate) {
		surgery_date=newSurgeryDate;
	}
	
	public String getSurgeryFacility() {  // Surgery Facility
		return facility_name;
	}
	public void setSurgeryFacility(String newFacility) {
		facility_name=newFacility;
	}
	
	public String getSurgeryCity() {  // Surgery City
		return city;
	}
	public void setSurgeryCity(String newCity) {
		city=newCity;
	}
	
	public String getSurgeryDetails() {  // Surgery Details
		return details;
	}
	public void setSurgeryDetails(String newDetail) {
		details=newDetail;
	}
	
	
	public String getSurgeryEffects() {  // Surgery Effects
		return side_effects;
	}
	public void setSurgeryEffects(String newEffect) {
		side_effects=newEffect;
	}
	
	public String getSurgeryOutcomes() {  // Surgery Outcomes
		return outcomes;
	}
	public void setSurgeryOutcomes(String newOutcome) {
		outcomes=newOutcome;
	}
	
	
	
	public int getPatientID() {  // EMAIL
		return patient_id;
	}
	public void setPatientID(int newPatientID) {
		patient_id = newPatientID;
	}
	//since it is auto increment
	
	
	public String getEmail() {  // EMAIL
		return email;
	}
	public void setEmail(String newEmail) {
		email = newEmail;
	}

	
	public String getPassword() { // PASSWORD
		return password;
	}
	public void setPassword(String newPassword) {
		password = newPassword;
	}


	public String getPhoneNumber() {  // PHONE NUMBER
		return phone_number;
	}
	public void setPhoneNumber(String newPhoneNumber) {
		phone_number = newPhoneNumber;
	}
	

	public String getFullName() {	//	 FULL NAME
		return full_name;
	}
	public void setFullName(String newFullName) {
		full_name = newFullName;
	}

	
	public String getAddress() {	//	 ADDRESS
		return address;
	}
	public void setAddress(String newAddress) {
		address = newAddress;
	}
	
	
	 public String getGender() {	//	 ADDRESS
	 	return gender;
	 }
	 public void setGender(String newGender) {
	 	gender = newGender;
	 }


	  public String getDOB() {	//	 ADDRESS
	  	return dob;
	  }
	  public void setDOB(String newDOB) {
	  	dob = newDOB;
	  }

	  public String getNaya() {	//	 If the guy is newly registered or not
	  	return naya;
	  }
	  public void setNaya(String newNaya) {
	  	naya = newNaya;
	  }

	  public boolean isValid() {
	  	return valid;
	  }

	  public void setValid(boolean newValid) {
	  	valid = newValid;
	  }

	  public boolean Registered() {
	  	return register;
	  }

	  public void setRegistered(boolean newregister) {
	  	register = newregister;
	  }
	}
