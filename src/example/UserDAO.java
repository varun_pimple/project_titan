package example;

import java.sql.*;
//import java.util.ArrayList;
import java.util.ArrayList;
//import javax.servlet.RequestDispatcher;

public class UserDAO {
	
	/**
	
	 This file talks to the Database, passes queries, gets ResultSet and sets values into UserBean

	*/
	static Connection currentCon = null;	//used
	static ResultSet rs = null; 		//used
	static ResultSet conditions =null;
	static ResultSet allergies =null;
	static ResultSet details =null;	//used
	static ResultSet surgery =null;
	static ResultSet alldetails =null;		//used
	static ResultSet appointments =null;
	static ResultSet precautions=null;
	static ArrayList<String> conditionList = new ArrayList<String>();		//conditions
	static ArrayList<String> allergyList = new ArrayList<String>();		//allergies
	static ArrayList<String> appointmentdateList = new ArrayList<String>();	
	static ArrayList<String> doctornameList = new ArrayList<String>();		
	static ArrayList<String> medicineList= new ArrayList<String>();	
	static ArrayList<String> precautionList = new ArrayList<String>();
	
	// static String[] conditionList =null; 
	// static String[] allergyList =null; 								// THIS GIVES NULL POINTER EXCEPTION
	
	public static UserBean login(UserBean bean) {


		//preparing some objects for connection 
		Statement stmt = null; 
		
		String email = bean.getEmail();    
		String password = bean.getPassword();
		//String naya = bean.getNaya();
		
		String searchQuery =
		"select * from User_Details where email='"+ email+ "' AND password='"+ password+ "'";
		
	 // "System.out.println" prints in the console; Normally used to trace the process
		System.out.println("Your Email is " + email);          
		System.out.println("Your password is " + password);
		System.out.println("Query: "+searchQuery);


		try 
		{
		//connect to DB 
			currentCon = ConnectionManager.getConnection();
			stmt=currentCon.createStatement();
			rs = stmt.executeQuery(searchQuery);	        
			boolean more = rs.next();

			if (!more) 
			{
				System.out.println("Sorry, you are not a registered user! Please sign up first");
				bean.setValid(false);
			} 
			
			else if (more) 
			{
		   //getting remaining fields
				int patient_id = rs.getInt("patient_id");
				String phone_number = rs.getString("phone_number");
				System.out.println("Welcome " + phone_number);

		   //Setting everything now
				bean.setPatientID(patient_id);
				bean.setPhoneNumber(phone_number);
				bean.setValid(true);
		   //SOME FIELDS LEFT
			}
		} 

		catch (Exception ex) 
		{
			System.out.println("Log In failed: An Exception has occurred! " + ex);
		} 
		
	 //some exception handling
		finally 
		{
			if (rs != null)	{
				try {
					rs.close();
				} catch (Exception e) {}
				rs = null;
			}

			if (stmt != null) {
				try {
					stmt.close();
				} catch (Exception e) {}
				stmt = null;
			}

			if (currentCon != null) {
				try {
					currentCon.close();
				} catch (Exception e) {
				}

				currentCon = null;
			}
		}
		return bean;	
	}	

	public static UserBean fillUp(UserBean bean){

		Statement stmt1 = null;
		Statement stmt2 = null;
		Statement stmt3 = null;
		Statement stmt4 = null;
		 //Statement stmt5 = null;
		 //Statement stmt6 = null;
		boolean b1 =false;
		boolean b2=false;
		boolean b3=false;

		int id= bean.getPatientID();
		/** String searchQuery2 =
				 " select * from User_Details u ,condition_allergy ca, surgeryrecord sr "
			   + "where u.patient_id=ca.patient_id AND ca.patient_id=sr.patient_id AND u.patient_id='"+id+"'; ";		
		*/ 
			   String searchQuery3 = "select * from User_Details where patient_id='"+ id+ "';";

			   String conditionQuery = "select * from condition_allergy where patient_id='"+ id+ "' AND choose='0' ;" ;
			   String allergyQuery = "select * from condition_allergy where patient_id='"+id+ "' AND choose='1' ;" ;
			   String appointment = "select * from doctorvisits where patient_id='"+id+"' order by date_of_visit" ;
		 //String appointmentPrec = "select medicine_precautions from doctorvisits where patient_id='"+id+"' AND choose='1' ";
			   try 
			   {
			//connect to DB 
			   	currentCon = ConnectionManager.getConnection();

			stmt1=currentCon.createStatement();					// For user details
			details = stmt1.executeQuery(searchQuery3);	       	 
			boolean more = details.next();
			
			stmt2=currentCon.createStatement();					// For conditions
			conditions = stmt2.executeQuery(conditionQuery);
			
			stmt3=currentCon.createStatement();					// For allergies
			allergies = stmt3.executeQuery(allergyQuery);
			
			stmt4=currentCon.createStatement();					// For appointments
			appointments = stmt4.executeQuery(appointment);
			
		  //  stmt5=currentCon.createStatement();					// for Precautions
		  //  precautions = stmt5.executeQuery(appointmentPrec);
			//boolean more1 = conditions.next();
			
			System.out.println("DAO -> fillup");
			if (!more) {
				System.out.println("Sorry, you are not a registered user! Please sign up first");
			   //bean.setValid(false);
			}    
			else if (more) {
				String full_name = details.getString("full_name");
				String address = details.getString("address");
				String gender = details.getString("gender");
				String dob = details.getString("dob");
				System.out.println("setting user details into bean");
				bean.setFullName(full_name);
				bean.setAddress(address);
				bean.setGender(gender);
				bean.setDOB(dob);
				System.out.println("bean filled");
			}
			while (conditions.next()) {				//conditions is result set
				System.out.println("inside condition loop");
				String condition = null;
				condition= conditions.getString(3); 
				System.out.println("got value from result, adding into array");
				conditionList.add(condition);		//adding into arraylist
				System.out.println("done adding into array");
				b1=true;
			}
			System.out.println("setting conditions into bean");
			if(b1)
				bean.setConditions(conditionList);
			
			while (allergies.next()){	
				String allergy = null;
				allergy = allergies.getString(3);
				allergyList.add(allergy);
				b2=true;
			}
			System.out.println("setting allergies into bean");
			if(b2)
				bean.setAllergies(allergyList);

			while(appointments.next()){
				String appointmentDate=null;
				String doctorName=null;
				String medicine=null;
				String precaution=null;	
				appointmentDate=appointments.getString(5);
				doctorName=appointments.getString(4);
				medicine=appointments.getString(6);
				precaution=appointments.getString(7);
				
				appointmentdateList.add(appointmentDate);
				doctornameList.add(doctorName);
				medicineList.add(medicine);
				precautionList.add(precaution);
			}
			if(b3){
				bean.setAppointment(appointmentdateList);
				bean.setDoctorName(doctornameList);
				bean.setMedicines(medicineList);
				bean.setPrecautions(precautionList);
			}
		}
		catch (Exception ex) 
		{
			System.out.println("Fill Up failed: An Exception has occurred! " + ex);
		}
		finally 
		{
			if (details != null)	{
				try {
					details.close();
				} catch (Exception e) {}
				details = null;
			}

			if (stmt1 != null) {
				try {
					stmt1.close();
				} catch (Exception e) {}
				stmt1 = null;
			}

			if (currentCon != null) {
				try {
					currentCon.close();
				} catch (Exception e) {
				}

				currentCon = null;
			}
		}
		return bean;

	}	

	public static UserBean completeProfile (UserBean bean){
		
		currentCon = ConnectionManager.getConnection();
		Statement st;
		try {
			st = currentCon.createStatement();

		//ResultSet rs;
			int i=0,j=0,m=0,n1=0,n2=0;
		// call a DAO function to do this
			System.out.println("DAO -> completeprofile");


			String email = bean.getEmail();
			String password = bean.getPassword();
			String address = bean.getAddress();    
			String fullname =bean.getFullName();
			String gender = bean.getGender();
			String dob = bean.getDOB();			
			ArrayList<String> conditions = bean.getConditions();
			ArrayList<String> allergies = bean.getAllergies();
   /*
		String[] surgery_name = bean.getSurgeryName();
		String[] surgery_date = bean.getSurgeryDate();
		String[] facility_name = bean.getSurgeryFacility();
		String[] city = bean.getSurgeryCity();
		String[] details = bean.getSurgeryDetails();
		String[] side_effects = bean.getSurgeryEffects();
		String[] outcomes = bean.getSurgeryOutcomes();
   */
		int patient_id=bean.getPatientID();
		n1=conditions.size();
		n2=allergies.size();
		
		
		try {
			i = st.executeUpdate(" UPDATE User_Details SET full_name='"+fullname+"',address='"+address+"', gender='"+gender+"',dob='"+dob+"' WHERE patient_id='"+patient_id+"';");
			for(int k=0;k<n1;k++){
				System.out.println("Inside FOR Chronic");
				String condition = conditions.get(k);
				j = st.executeUpdate(" INSERT INTO CONDITION_ALLERGY(patient_id,condition_allergy,sequence_no,CHOOSE) VALUES ('"+patient_id+"','"+condition+"','1','0')");
				System.out.println("Ho gaya");
				System.out.println(j);
			}
			for(int l=0;l<n2;l++){
				System.out.println("Inside FOR Allergy");
				String allergy = allergies.get(l);
				m = st.executeUpdate(" INSERT INTO CONDITION_ALLERGY(patient_id,condition_allergy,sequence_no,CHOOSE) VALUES ('"+patient_id+"','"+allergy+"','1','1')");
				System.out.println("Ho gaya");
				System.out.println(m);
			}
			//Surgery SQL Query wont be from this function
			// p=st.executeUpdate("INSERT INTO SurgeryRecord(patient_id,name_of_surgery,facility_name,city,date_performed,details,sideEffects,outcome) VALUES ('"+patient_id+"','"+surgery_name+"','"+facility_name+"','"+city+"','"+surgery_date+"','"+details+"','"+side_effects+"','"+outcomes+"')");
		}	 
		catch (SQLException ex)
		{
			ex.printStackTrace();
		}
		if (i >0 && j>0 && m >0) {
		   // response.sendRedirect("profilecopy.jsp");
			System.out.println("Inside CompleteProfile and Queries have run successfully");
			System.out.println(email);
			System.out.println(password);
		}
	} catch (SQLException e) {
			// TODO Auto-generated catch block
		e.printStackTrace();
	}
	return bean;
}

public static UserBean register(UserBean bean){

	currentCon = ConnectionManager.getConnection();
	Statement st;

	try {
		st = currentCon.createStatement();

		String email = bean.getEmail();
		String pwd = bean.getPassword();
		String mobno= bean.getPhoneNumber();
		
		
		System.out.println("DAO -> register");
		try {
			int i = st.executeUpdate(" insert into USER_DETAILS(email, phone_number, password) values('"+email+"','"+mobno+"','"+pwd+"') ");
			if (i > 0) {
				bean.setRegistered(true);
			} else {
				bean.setRegistered(false);
			}

		}	 
		catch (SQLException ex)
		{
			ex.printStackTrace();
		}
	} catch (SQLException e) {
			// TODO Auto-generated catch block
		e.printStackTrace();
	}  
	return bean;

}

public static UserBean updateDetails(UserBean bean){

	String email=bean.getEmail();
	String address=bean.getAddress();
	String phone=bean.getPhoneNumber();
	int patient_id=bean.getPatientID();
	System.out.println("In the bean to update");
	System.out.println(email);
	System.out.println(address);
	currentCon = ConnectionManager.getConnection();
	Statement st3;
		//String Query=" UPDATE user_details SET email='"+email+"', address='"+address+"',phone_number='"+phone+"' WHERE patient_id='"+patient_id+"'; ";
	try{
		st3=currentCon.createStatement();
		System.out.println("In TRY");
		int c = st3.executeUpdate(" UPDATE user_details SET email='"+email+"', address='"+address+"',phone_number='"+phone+"' WHERE patient_id='"+patient_id+"'; ");
		if(c>0){
			System.out.println("Updated Details");
		}
	}
	catch (SQLException ex)
	{
		ex.printStackTrace();
	}

	return bean;
}

public static UserBean updateConditions(UserBean bean) {
		// TODO Auto-generated method stub
	ArrayList<String> conditions = bean.getConditions();
	int i,j;
	i=0;
	int n = conditions.size();
	System.out.println("COndition Length in DAO is"+n);
	int patient_id=bean.getPatientID();
	Statement st4;
	try {
		currentCon = ConnectionManager.getConnection();

		st4=currentCon.createStatement();
		i = st4.executeUpdate(" delete from condition_allergy where patient_id="+patient_id+" and CHOOSE ='0';");
		System.out.println("Deleted all conditions ="+i);
		for(int k=0;k<n;k++){
			System.out.println("Inside Insertion Query");
			String condition = conditions.get(k);
			j = st4.executeUpdate(" INSERT INTO CONDITION_ALLERGY(patient_id,condition_allergy,sequence_no,CHOOSE) VALUES ('"+patient_id+"','"+condition+"','1','0')");
			System.out.println("Ho gaya conditions insert");
			System.out.println(j);
		}

	}	 
	catch (SQLException ex)
	{
		ex.printStackTrace();
	}
	return bean;
}

public static UserBean updateAllergies(UserBean bean) {
		// TODO Auto-generated method stub
	ArrayList<String> allergies = bean.getAllergies();
	int i,j;
	i=0;
	int n = allergies.size();
	System.out.println("Allergies Length in DAO is"+n);
	int patient_id=bean.getPatientID();
	Statement st4;
	try {
		currentCon = ConnectionManager.getConnection();

		st4=currentCon.createStatement();
		i = st4.executeUpdate(" delete from condition_allergy where patient_id="+patient_id+" and CHOOSE ='1';");
		System.out.println("Deleted all allergies ="+i);
		for(int k=0;k<n;k++){
			System.out.println("Inside Insertion Query");
			String condition = allergies.get(k);
			j = st4.executeUpdate(" INSERT INTO CONDITION_ALLERGY(patient_id,condition_allergy,sequence_no,CHOOSE) VALUES ('"+patient_id+"','"+condition+"','1','1')");
			System.out.println("Ho gaya allergies insert");
			System.out.println(j);
		}

	}	 
	catch (SQLException ex)
	{
		ex.printStackTrace();
	}
	return bean;
}

public static UserBean addsurgery(UserBean bean) {
		// TODO Auto-generated method stub
	String surgery_name = bean.getSurgeryName();
	String surgery_date = bean.getSurgeryDate();
	String facility_name = bean.getSurgeryFacility();
	String city = bean.getSurgeryCity();
	String details = bean.getSurgeryDetails();
	String side_effects = bean.getSurgeryEffects();
	String outcomes = bean.getSurgeryOutcomes();
	int patient_id=bean.getPatientID();
	Statement st4;
	try {
		currentCon = ConnectionManager.getConnection();
		st4=currentCon.createStatement();
		String query="INSERT INTO SurgeryRecord(patient_id,name_of_surgery,facility_name,city,date_performed,details,sideEffects,outcome) VALUES ('"+patient_id+"','"+surgery_name+"','"+facility_name+"','"+city+"','"+surgery_date+"','"+details+"','"+side_effects+"','"+outcomes+"')";
		int p = st4.executeUpdate(query);	         
		System.out.println("Added a sugery record = "+p);

	}	 
	catch (SQLException ex)
	{
		ex.printStackTrace();
	}
	return bean;
}

public static UserBean addappointment(UserBean bean) {
		// TODO Auto-generated method stub
	System.out.println("In DAO to add appointment");
	ArrayList<String> doctor_name = bean.getDoctorName();
	ArrayList<String> app_date = bean.getAppointment();
	ArrayList<String> medicines = bean.getMedicines();
	ArrayList<String> precautions = bean.getPrecautions();
	int patient_id=bean.getPatientID();
	Statement st4;
	try {
		currentCon = ConnectionManager.getConnection();
		st4=currentCon.createStatement();
		String query1="INSERT INTO DoctorVisits(patient_id,doctor_name,date_of_visit,medicine,precaution) VALUES ('"+patient_id+"','"+doctor_name.get(0)+"','"+app_date.get(0)+"','"+medicines.get(0)+"','"+precautions.get(0)+"')";
		int p1 = st4.executeUpdate(query1);    
		System.out.println("Added a sugery record = "+p1+" and nothing");

	}	 
	catch (SQLException ex)
	{
		ex.printStackTrace();
	}
	return bean;
}


}


